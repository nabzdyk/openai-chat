import { Prodia } from '@/types/api-prodia';
import { OpenAIAPI } from '@/types/api-openai';
import { createDMessage, DMessage, useChatStore } from '@/lib/stores/store-chats';
import { useSettingsStore } from '@/lib/stores/store-settings';


export const prodiaDefaultModelId: string = 'v1-5-pruned-emaonly.ckpt [81761151]';

export interface ApiImgResponse {
  message: OpenAIAPI.Image.CompletionsResponse;
}

/**
 * The main 'image generation' function - for now specialized to the 'imagine' command.
 */
export const runImageGenerationUpdatingState = async (conversationId: string, history: DMessage[], imageText: string) => {

  // reference the state editing functions
  const { editMessage, setMessages } = useChatStore.getState();

  // create a blank and 'typing' message for the assistant
  let assistantMessageId: string;
  {
    const assistantMessage: DMessage = createDMessage('assistant', `Give me a few seconds while I draw ${imageText?.length > 20 ? 'that' : '"' + imageText + '"'}...`);
    assistantMessageId = assistantMessage.id;
    assistantMessage.typing = true;
    assistantMessage.purposeId = history[0].purposeId;
    assistantMessage.originLLM = 'DALL-E';
    setMessages(conversationId, [...history, assistantMessage]);
  }

  // generate the image
  const { prodiaApiKey: apiKey, prodiaModelId, prodiaNegativePrompt: negativePrompt, prodiaSteps: steps, prodiaCfgScale: cfgScale, prodiaSeed: seed } = useSettingsStore.getState();
  const input: OpenAIAPI.Image.CompletionsRequest = {
    prompt: imageText,
  };

  try {
    //API CALL
    const response = await fetch('/api/openai/dall-e', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(input),
    });

    // if (response.ok) {
      //API RESPONSE
      const imagineResponse: ApiImgResponse = await response.json();

      //MOCK RESPONSE
      // const imagineResponse: ApiImgResponse = {"message":{"created":1683149791,"data":[{"url":"https://oaidalleapiprodscus.blob.core.windows.net/private/org-ti9RMsRq1vsYlxRLHkrKm7fb/user-dPkMvlBGTSKWKslkBxbcc282/img-USIHZhaS1QaSvZMtqUY7XW2q.png?st=2023-05-03T20%3A36%3A31Z&se=2023-05-03T22%3A36%3A31Z&sp=r&sv=2021-08-06&sr=b&rscd=inline&rsct=image/png&skoid=6aaadede-4fb3-4698-a8f6-684d7786b067&sktid=a48cca56-e6da-484e-a814-9c849652bcb3&skt=2023-05-03T12%3A15%3A00Z&ske=2023-05-04T12%3A15%3A00Z&sks=b&skv=2021-08-06&sig=a0Jh1wdXd0cytCWRZsXnKXATIKb1mtHfD7AaOBV7J04%3D"}]}}
      // edit the assistant message to be the image
      // if (imagineResponse.status === 'success') {
        console.log("imagineResponse: ", imagineResponse);
        editMessage(conversationId, assistantMessageId, { text: imagineResponse.message.data[0].url }, false);
        // NOTE: imagineResponse shall have an altText which contains some description we could show on mouse hover
        //       Would be hard to do it with the current plain-text URL tho - shall consider changing the workaround format
      // }
    // } else
    //   editMessage(conversationId, assistantMessageId, { text: `Sorry, I had issues requesting this image. Check your API key?` }, false);
  } catch (error: any) {
    editMessage(conversationId, assistantMessageId, { text: `Sorry, I couldn't generate an image for that. Issue: ${error}.` }, false);
  }
  editMessage(conversationId, assistantMessageId, { typing: false }, false);
};
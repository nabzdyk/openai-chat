import { NextRequest, NextResponse } from 'next/server';

import { OpenAI } from "langchain/llms/openai";
import { ChatOpenAI } from "langchain/chat_models/openai";
import { HumanChatMessage, SystemChatMessage } from "langchain/schema";

import {
    SystemMessagePromptTemplate,
    HumanMessagePromptTemplate,
    ChatPromptTemplate,
} from "langchain/prompts";



// const chat = new ChatOpenAI({ openAIApiKey: process.env.OPENAI_API_KEY,temperature: 0 });

// const translationPrompt = ChatPromptTemplate.fromPromptMessages([
// SystemMessagePromptTemplate.fromTemplate(
//     "You are a helpful assistant that translates {input_language} to {output_language}."
// ),
// HumanMessagePromptTemplate.fromTemplate("{text}"),
// ]);


export default async function handler(req: NextRequest) {
    try {

      return new NextResponse(JSON.stringify({
        message: "test 123",
      } ));
    } catch (error: any) {
      console.error('Fetch request failed:', error);
      return new NextResponse(`[Issue] ${error}`, { status: 400 });
    }
}

// noinspection JSUnusedGlobalSymbols
export const config = {
    runtime: 'edge',
  };
import { NextRequest, NextResponse } from 'next/server';

import { OpenAIAPI } from '@/types/api-openai';


if (!process.env.OPENAI_API_KEY)
  console.warn(
    'OPENAI_API_KEY has not been provided in this deployment environment. ' +
    'Will use the optional keys incoming from the client, which is not recommended.',
  );


// helper functions

export async function extractOpenaiImageInputs(req: NextRequest): Promise<ApiChatInput> {
  const {
    api: userApi = {},
    model,
    prompt,
    n = 2,
    size = "1024x1024"
  } = (await req.json()) as Partial<ApiChatInput>;
  if ( !prompt)
    throw new Error('Missing required parameters: prompt');

  const api: OpenAIAPI.Configuration = {
    apiKey: ( process.env.OPENAI_API_KEY || '').trim(),
    apiHost: ( process.env.OPENAI_API_HOST || 'api.openai.com').trim().replaceAll('https://', ''),
    apiOrganizationId: (process.env.OPENAI_API_ORG_ID || '').trim(),
  };
  if (!api.apiKey)
    throw new Error('Missing OpenAI API Key. Add it on the client side (Settings icon) or server side (your deployment).');

  return { api, prompt, n:2, size: "1024x1024" };
}

const openAIHeaders = (api: OpenAIAPI.Configuration): HeadersInit => ({
  'Content-Type': 'application/json',
  Authorization: `Bearer ${api.apiKey}`,
  ...(api.apiOrganizationId && { 'OpenAI-Organization': api.apiOrganizationId }),
});

export const chatCompletionPayload = (input: Omit<ApiChatInput, 'api'>, stream: boolean): OpenAIAPI.Image.CompletionsRequest => ({
  prompt: input.prompt,
  size: input.size,
  n: 1,
});

async function rethrowOpenAIError(response: Response) {
  if (!response.ok) {
    let errorPayload: object | null = null;
    try {
      errorPayload = await response.json();
    } catch (e) {
      // ignore
    }
    throw new Error(`${response.status} · ${response.statusText}${errorPayload ? ' · ' + JSON.stringify(errorPayload) : ''}`);
  }
}


export async function postToOpenAI<TBody extends object>(api: OpenAIAPI.Configuration, apiPath: string, body: TBody, signal?: AbortSignal): Promise<Response> {
  console.log("postToOpenAI body: ", body)
  const response = await fetch(`https://${api.apiHost}${apiPath}`, {
    method: 'POST',
    headers: openAIHeaders(api),
    body: JSON.stringify(body),
    signal,
  });
  await rethrowOpenAIError(response);
  return response;
}


// I/O types for this endpoint

export interface ApiChatInput {
  api: OpenAIAPI.Configuration;
  model?: string;
  prompt: string;
  n?: number;
  size: string;
}

export interface ApiChatResponse {
  message: any;
}

export default async function handler(req: NextRequest) {
    try {
      const { api, ...rest } = await extractOpenaiImageInputs(req);
      const response = await postToOpenAI(api, '/v1/images/generations', chatCompletionPayload(rest, false));
      const completion: OpenAIAPI.Image.CompletionsResponse = await response.json();
      console.log('completion: ', completion);
      return new NextResponse(JSON.stringify({
        message: completion,
      } as ApiChatResponse));
    } catch (error: any) {
      console.error('Fetch request failed:', error);
      return new NextResponse(`[Issue] ${error}`, { status: 400 });
    }
  }
  
  // noinspection JSUnusedGlobalSymbols
  export const config = {
    runtime: 'edge',
  };
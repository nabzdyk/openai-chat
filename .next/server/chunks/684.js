"use strict";
exports.id = 684;
exports.ids = [684];
exports.modules = {

/***/ 9282:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "H": () => (/* binding */ Brand)
/* harmony export */ });
/**
 * Application Identity (Brand)
 *
 * Also note that the 'Brand' is used in the following places:
 *  - README.md             all over
 *  - package.json          app-slug and version
 *  - public/manifest.json  name, short_name, description, theme_color, background_color
 */ const Brand = {
    // Name: 'big-AGI',
    // UpperName: 'BIG-AGI',
    Title: {
        Common: ( false ? 0 : "") + "big-AGI"
    },
    Meta: {
        SiteName: "big-AGI",
        Title: "big-AGI: Personal AGI App",
        Description: "big-AGI is a free, open-source project to build a general artificial intelligence (AGI) that can solve any problem.",
        Keywords: "artificial general intelligence, agi, openai, gpt-4, ai personas, code execution, pdf import, voice i/o, ai chat, artificial intelligence",
        ThemeColor: "#434356",
        TwitterSite: "@enricoros"
    },
    URIs: {
        // Slug: 'big-agi',
        Home: "https://big-agi.com",
        CardImage: "https://big-agi.com/icons/card-dark-1200.png",
        OpenRepo: "https://github.com/enricoros/big-agi"
    }
};


/***/ }),

/***/ 2341:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "S9": () => (/* binding */ createEmotionCache),
/* harmony export */   "Xg": () => (/* binding */ cssRainbowColorKeyframes),
/* harmony export */   "Zf": () => (/* binding */ bodyFontClassName),
/* harmony export */   "rS": () => (/* binding */ theme)
/* harmony export */ });
/* harmony import */ var next_font_google_target_css_path_lib_theme_ts_import_Inter_arguments_weight_400_500_600_700_subsets_latin_display_swap_fallback_Helvetica_Arial_sans_serif_variableName_inter___WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(642);
/* harmony import */ var next_font_google_target_css_path_lib_theme_ts_import_Inter_arguments_weight_400_500_600_700_subsets_latin_display_swap_fallback_Helvetica_Arial_sans_serif_variableName_inter___WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_font_google_target_css_path_lib_theme_ts_import_Inter_arguments_weight_400_500_600_700_subsets_latin_display_swap_fallback_Helvetica_Arial_sans_serif_variableName_inter___WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_font_google_target_css_path_lib_theme_ts_import_JetBrains_Mono_arguments_weight_400_500_600_700_subsets_latin_display_swap_fallback_monospace_variableName_jetBrainsMono___WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(7639);
/* harmony import */ var next_font_google_target_css_path_lib_theme_ts_import_JetBrains_Mono_arguments_weight_400_500_600_700_subsets_latin_display_swap_fallback_monospace_variableName_jetBrainsMono___WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_font_google_target_css_path_lib_theme_ts_import_JetBrains_Mono_arguments_weight_400_500_600_700_subsets_latin_display_swap_fallback_monospace_variableName_jetBrainsMono___WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _emotion_cache__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1913);
/* harmony import */ var _emotion_cache__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_emotion_cache__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _emotion_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2805);
/* harmony import */ var _emotion_react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_emotion_react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _mui_joy__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5436);
/* harmony import */ var _mui_joy__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_mui_joy__WEBPACK_IMPORTED_MODULE_2__);





const theme = (0,_mui_joy__WEBPACK_IMPORTED_MODULE_2__.extendTheme)({
    fontFamily: {
        body: (next_font_google_target_css_path_lib_theme_ts_import_Inter_arguments_weight_400_500_600_700_subsets_latin_display_swap_fallback_Helvetica_Arial_sans_serif_variableName_inter___WEBPACK_IMPORTED_MODULE_3___default().style.fontFamily),
        code: (next_font_google_target_css_path_lib_theme_ts_import_JetBrains_Mono_arguments_weight_400_500_600_700_subsets_latin_display_swap_fallback_monospace_variableName_jetBrainsMono___WEBPACK_IMPORTED_MODULE_4___default().style.fontFamily)
    },
    colorSchemes: {
        light: {
            palette: {
                background: {
                    body: "var(--joy-palette-neutral-300, #B9B9C6)"
                },
                primary: {
                    // 50: '#F4FAFF', // softBg
                    100: "#f0f8ff"
                },
                neutral: {
                    solidBg: "var(--joy-palette-neutral-700, #434356)",
                    solidHoverBg: "var(--joy-palette-neutral-800, #25252D)"
                }
            }
        },
        dark: {
            palette: {
                background: {
                    surface: "var(--joy-palette-neutral-900, #131318)",
                    level1: "var(--joy-palette-common-black, #09090D)",
                    level2: "var(--joy-palette-neutral-800, #25252D)"
                }
            }
        }
    }
});
const bodyFontClassName = (next_font_google_target_css_path_lib_theme_ts_import_Inter_arguments_weight_400_500_600_700_subsets_latin_display_swap_fallback_Helvetica_Arial_sans_serif_variableName_inter___WEBPACK_IMPORTED_MODULE_3___default().className);
const cssRainbowColorKeyframes = _emotion_react__WEBPACK_IMPORTED_MODULE_1__.keyframes`
  100%, 0% {
    color: rgb(255, 0, 0);
  }
  8% {
    color: rgb(204, 102, 0);
  }
  16% {
    color: rgb(128, 128, 0);
  }
  25% {
    color: rgb(77, 153, 0);
  }
  33% {
    color: rgb(0, 179, 0);
  }
  41% {
    color: rgb(0, 153, 82);
  }
  50% {
    color: rgb(0, 128, 128);
  }
  58% {
    color: rgb(0, 102, 204);
  }
  66% {
    color: rgb(0, 0, 255);
  }
  75% {
    color: rgb(127, 0, 255);
  }
  83% {
    color: rgb(153, 0, 153);
  }
  91% {
    color: rgb(204, 0, 102);
  }`;
// Emotion Cache (with insertion point on the SSR pass)
const isBrowser = typeof document !== "undefined";
function createEmotionCache() {
    let insertionPoint;
    if (isBrowser) {
        // On the client side, _document.tsx has a meta tag with the name "emotion-insertion-point" at the top of the <head>.
        // This assures that MUI styles are loaded first, and allows allows developers to easily override MUI styles with other solutions like CSS modules.
        const emotionInsertionPoint = document.querySelector('meta[name="emotion-insertion-point"]');
        insertionPoint = emotionInsertionPoint ?? undefined;
    }
    return _emotion_cache__WEBPACK_IMPORTED_MODULE_0___default()({
        key: "mui-style",
        insertionPoint
    });
}


/***/ })

};
;